package com.example.fileobserver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

public class LoggerGF implements ReadLogger {

	private String charBeginLine = "";
	private String charEndLine = "\n";
	private boolean isGf = false;
	private long previousLength;
	private String filename;
	private RandomAccessFile fileCursor;
	private String charset;
	private List<String> stack = new LinkedList<>();

	public LoggerGF(String filenameIn) throws FileNotFoundException {
		this.filename = filenameIn;
		charset = "CP1251";
		init();
	}

	public LoggerGF(String filenameIn, String charsetName) throws FileNotFoundException, UnsupportedEncodingException {
		filename = filenameIn;
		charset = charsetName;
		init();
	}

	private void init() throws FileNotFoundException {
		fileCursor = new RandomAccessFile(filename, "r");
		if (filename.toUpperCase().contains("SERVER")) {
			charBeginLine = "[#|";
			charEndLine = "|#]";
			isGf = true;
		}

	}

	@Override
	public String getMessage() throws IOException {

		boolean isMessageFound = false;
		boolean isBegin = false;

		StringBuffer message = new StringBuffer();

		while (!isMessageFound) {
			String line = getLine();

			if (line.length() == 0) {
				return "";
			}

			if (lineIsFull(line)) {
				return line + "\n";
			}

			if (!isBegin) {
				if (foundBeginMessage(line)) {
					isBegin = true;
				} else {
					continue;
				}
			}

			if (isFoundEndMessage(line)) {
				isMessageFound = true;
			}

			message.append(line);
			message.append("\n");
		}

		return message.toString();
	}

	private boolean foundBeginMessage(String line) {
		return (line.indexOf(charBeginLine) >= 0) ? true : false;
	}

	private boolean isFoundEndMessage(String line) {
		return (line.indexOf(charEndLine) >= 0) ? true : false;
	}

	private boolean lineIsFull(String line) {
		if (line.indexOf(charBeginLine) == 0 && line.indexOf(charEndLine) > 0) {
			return true;
		}
		if (!isGf) {
			return true;
		}
		return false;
	}

	private String getLine() throws IOException {
		if (stack.size() == 0) {
			readMessageByByte();
			if (stack.size() == 0) {
				return "";
			}
		}
		return stackPoll();
	}

	private String stackPoll() {
		String line = stack.get(0);
		stack.remove(0);
		return line;
	}

	private void readMessageByByte() throws IOException {
		long sizeBuf = 0;
		long newSizeeBuf = fileCursor.length() - fileCursor.getFilePointer();
		if (newSizeeBuf > 0) {
			sizeBuf = newSizeeBuf;
		}

		if (sizeBuf == 0) {
			return;
		}

		byte[] byteBuffer = new byte[(int) sizeBuf];
		fileCursor.read(byteBuffer);
		String res = new String(byteBuffer, charset);
		for (String line : res.split("\n")) {
			stack.add(line);
		}
		return;
	}

	@Override
	public boolean hasNext() throws IOException {
		if (fileCursor.length() > fileCursor.getFilePointer() || stack.size() > 0) {
			return true;
		}
		checkRollingFile();
		return false;
	}

	private void checkRollingFile() throws IOException {
		long currentLength = fileCursor.length();

		if (currentLength < previousLength) {
			System.out.println("Rolling file");

			if (previousLength - currentLength < 1_000_000) {
				waitSecond();
			}

			long currentFileSize = fileCursor.length();
			if (currentFileSize < previousLength) {
				fileCursor.close();
				fileCursor = new RandomAccessFile(filename, "r");
			}

		}
		previousLength = currentLength;
	}

	protected void waitSecond() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() throws IOException {
		fileCursor.close();
	}

	@Override
	public void goToEndFile() {
		try {
			fileCursor.seek(fileCursor.length());
		} catch (IOException e) {
			System.out.println("error=" + e.getMessage());
		}
	}

}
