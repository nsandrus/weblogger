package com.example.services;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.example.fileobserver.FileObserver;

@ServerEndpoint(value = "/logservice")
public class BroadcastServerEndpoint {
	private static final Map<Session, Client> map = new ConcurrentHashMap<>();

	@Autowired
	ApplicationContext appcontext;

	@OnOpen
	public void onOpen(final Session session) {
		map.put(session, new Client());
	}

	@OnClose
	public void onClose(final Session session) {
		map.remove(session);
	}

	@OnMessage
	public void onMessage(final String message, final Session client) throws IOException, EncodeException {

		if (message.startsWith("start")) {

			Client clientMap = map.get(client);
			if (clientMap.getFileObserver() != null) {
				return;
			}
			FileObserver fileObserver = new FileObserver();
			fileObserver.initialization(client, clientMap);
			clientMap.setFileObserver(fileObserver);
			Thread thread = new Thread(fileObserver);
			thread.start();

		}

		if (message.startsWith("file:")) {
			Client clientMap = map.get(client);
			clientMap.setFilename(message.substring(5));
			stopThread(client);
		}

		if (message.startsWith("filter:")) {
			Client clientMap = map.get(client);
			clientMap.setFilter(message.substring(7));
			clientMap.getFilterMain().setNewFilter(message.substring(7));
		}

		if (message.startsWith("stop")) {
			stopThread(client);
		}

	}

	private void stopThread(final Session client) {
		Client clientMap = map.get(client);
		if (clientMap.getFileObserver() == null) {
			return;
		}

		clientMap.getFileObserver().stopObserve();
		clientMap.setFileObserver(null);
	}
}