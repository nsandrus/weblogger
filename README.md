### Jetty 9.1 and JSR-356 (Java API for WebSockets)

# To build:
 - mvn clean package
 
# To run the server:
 - java -jar target\jetty-web-sockets-jsr356-0.0.1-SNAPSHOT-server.jar 
 
# Go to http://localhost:8080/
  
  