package com.example.services;

import com.example.fileobserver.FileObserver;
import com.example.filter.Filter;
import com.example.filter.FilterSimple;

public class Client {

	private String filename;
	private String filter;
	private FileObserver fileObserver;
	private Filter filterMain = new FilterSimple();

	public Filter getFilterMain() {
		return filterMain;
	}

	public void setFilterMain(Filter filterMain) {
		this.filterMain = filterMain;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public FileObserver getFileObserver() {
		return fileObserver;
	}

	public void setFileObserver(FileObserver fileObserver) {
		this.fileObserver = fileObserver;
	}

}
