package com.example.filter;

import java.io.IOException;
import java.util.List;

public interface Filter {

	void setParam(String filter2);

	boolean isPassed(String lineLog);

	public List<String> getListOr();

	public List<String> getListAnd();

	public List<String> getListNot();

	boolean doWork(String lineLog) throws IOException;

	void setNewFilter(String parameter);

}
