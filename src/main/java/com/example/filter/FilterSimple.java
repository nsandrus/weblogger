package com.example.filter;

import static com.example.filter.FilterSimple.TypeWord.AND;
import static com.example.filter.FilterSimple.TypeWord.NOT;
import static com.example.filter.FilterSimple.TypeWord.OR;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FilterSimple implements Filter {

	enum TypeWord {
		OR, AND, NOT
	};

	List<String> listOr = new ArrayList<>();
	List<String> listAnd = new ArrayList<>();
	List<String> listNot = new ArrayList<>();

	@Override
	public void setParam(String filter2) {
		listOr.clear();
		listAnd.clear();
		listNot.clear();
		String[] arrayWords = filter2.split(" ");
		boolean isBlockText = false;
		String blockText = "";
		TypeWord type = null;

		for (String word : arrayWords) {
			if (!isBlockText) {
				if (word.startsWith("&")) {
					type = AND;
					word = word.substring(1);
				} else if (word.startsWith("^")) {
					type = NOT;
					word = word.substring(1);
				} else {
					type = OR;
				}
				if (word.equals("(")) {
					isBlockText = true;
					continue;
				}

				if (type.equals(OR)) {
					listOr.add(word);
				} else if (type.equals(AND)) {
					listAnd.add(word);
				} else if (type.equals(NOT)) {
					listNot.add(word);
				}
			}
			if (isBlockText) {
				if (word.equals(")")) {
					isBlockText = false;
					blockText = blockText.substring(0, blockText.length() - 1);

					if (type.equals(OR)) {
						listOr.add(blockText);
					} else if (type.equals(AND)) {
						listAnd.add(blockText);
					} else if (type.equals(NOT)) {
						listNot.add(blockText);
					}
					blockText = "";
					continue;
				}
				blockText += word + " ";

			}

		}

	}

	public List<String> getListOr() {
		return listOr;
	}

	public List<String> getListAnd() {
		return listAnd;
	}

	public List<String> getListNot() {
		return listNot;
	}

	@Override
	public boolean isPassed(String lineLog) {
		for (String word : listAnd) {
			if (!lineLog.contains(word)) {
				return false;
			}
		}

		for (String word : listNot) {
			if (lineLog.contains(word)) {
				return false;
			}
		}

		for (String word : listOr) {
			if (lineLog.contains(word)) {
				return true;
			}
		}

		if (listOr.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean doWork(String lineLog) throws IOException {
		return isPassed(lineLog);
	}

	@Override
	public void setNewFilter(String parameter) {
		setParam(parameter);

	}

}
