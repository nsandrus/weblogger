package com.example.fileobserver;

import static com.example.fileobserver.StateFileObserver.READY;
import static com.example.fileobserver.StateFileObserver.RUN;
import static com.example.fileobserver.StateFileObserver.STOP;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.websocket.EncodeException;
import javax.websocket.Session;

import com.example.color.MessageColorHTML;
import com.example.services.Client;

public class FileObserver implements Runnable {

	private String filename;
	private String codepage = "CP1251";
	private volatile StateFileObserver stateWork = STOP;
	private volatile List<String> results = new CopyOnWriteArrayList<String>();

	private Session session;
	private Client client;

	public FileObserver() {
		stateWork = STOP;
	}

	public void run() {
		if (!stateWork.equals(READY)) {
			return;
		}
		try {

			if (filename == null) {
				session.getBasicRemote().sendObject("<div class='errorEmitter'>Не указано имя файла =" + filename + "</div");
				return;
			}
			session.getBasicRemote().sendObject("<div class='welcome'>Запущен поток для чтения файла =" + filename + "</div>");
		} catch (IOException | EncodeException e1) {
			System.out.println("error:" + e1.getMessage());
			sendError(e1);
		}
		stateWork = RUN;

		try {
			mainCycle();
		} catch (Exception e) {
			System.out.println("error:" + e.getMessage());
			sendError(e);
		}
		client.setFileObserver(null);
	}

	private void sendError(Exception e2) {
		try {
			session.getBasicRemote().sendObject("<div class='error' >" + e2.getMessage() + " </div>");
		} catch (IOException | EncodeException e1) {
			System.out.println("error send:" + e1.getMessage());
		}
	}

	private void mainCycle() throws FileNotFoundException, UnsupportedEncodingException, EncodeException {
		ReadLogger readerGf = getInstanceReader();
		readerGf.goToEndFile();
		MessageColorHTML colmes = new MessageColorHTML();

		while (!stateWork.equals(STOP)) {
			try {
				if (readerGf.hasNext()) {
					String lineLog = readerGf.getMessage();
					if (stateWork.equals(RUN)) {
						if (client.getFilterMain().isPassed(lineLog)) {
							String colorMessage = colmes.getMessageColorized(lineLog);
							session.getBasicRemote().sendObject(colorMessage);
						}
					}

				} else {
					sleep(50);
				}
			} catch (IOException e) {
				sleep(300);

			} catch (IllegalStateException e3) {
				stateWork = STOP;
			}
		}

	}

	public void clear(String analize) {
		this.results.clear();
	}

	private ReadLogger getInstanceReader() throws FileNotFoundException, UnsupportedEncodingException {
		ReadLogger readerGf = null;
		readerGf = new LoggerGF(filename, codepage);
		return readerGf;
	}

	private void sleep(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
		}
	}

	public synchronized void initialization(Session client2, Client clientMap) {
		this.filename = clientMap.getFilename();
		this.session = client2;
		this.client = clientMap;

		if (!stateWork.equals(STOP)) {
			return;
		}

		stateWork = READY;
		results.clear();
	}

	public synchronized void stopObserve() {
		stateWork = STOP;
	}

	public String getNewLines() {
		int len = results.size();
		if (len <= 0) {
			return "";
		}
		StringBuilder batchText = new StringBuilder();
		for (int i = 0; i < len; i++) {
			batchText.append(results.get(i));
		}
		for (int i = 0; i < len; i++) {
			results.remove(0);
		}
		return batchText.toString();
	}

}
