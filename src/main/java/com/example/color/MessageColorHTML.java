package com.example.color;

public class MessageColorHTML {

	public String getMessageColorized(String message) {

		String escaped = org.apache.commons.lang3.StringEscapeUtils.escapeHtml4(message);
		String line = escaped.replaceAll("\r", "");
		line = line.replaceAll("\n", "<br/>");
		line = line.replaceAll("\t", "&emsp;");
		line = line.replaceAll("  ", "&nbsp;&nbsp;");

		String[] chunks = line.split("\\|");
		int countColumn = chunks.length - 1;
		StringBuilder result = new StringBuilder();
		int column = 0;
		String tag = "div";
		if (chunks[0].equals("[#") && countColumn > 1) {
			tag = "p";
		}

		result.append("<" + tag + " class=\"gf\">");
		for (String chunk : chunks) {

			if (column == 2) {
				result.append("<span id=\"" + chunk + "\">");
			} else if (countColumn > 8 && column >= 6) {
				result.append("<span id=\"column6\">");
			} else {
				result.append("<span id=\"column" + column + "\">");
			}

			result.append(chunk);
			result.append("</span>");

			if (countColumn != column) {
				result.append("|");
			}
			column++;
		}
		result.append("</" + tag + ">");

		return result.toString();
	}
}