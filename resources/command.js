var socket;




function Start() {
	socket.send('start');
}


function Stop() {
	socket.send('stop');
}


		function Clear() {
			console.log("clear page");
			$('#mainest').empty().append('<div id="footer2"></div>');
		};


		$(document).ready(function() {
			(window.onpopstate = function() {
				var match, pl = /\+/g, // Regex for replacing addition symbol with a space
				search = /([^&=]+)=?([^&]*)/g, decode = function(s) {
					return decodeURIComponent(s.replace(pl, " "));
				}, query = window.location.search.substring(1);

				urlParams = {};
				while (match = search.exec(query))
					urlParams[decode(match[1])] = decode(match[2]);
			})();
			$('#filter').val(urlParams["filter"]);
			$('#filename').val(urlParams["filename"]);
			$('#selectfile').val(urlParams["filename"])

			$("#selectfile").change(
					function() {
						$('#filename').val($('#selectfile').val())
						socket.send('file:' + $('#filename').val());
					});

			$("#filename").change(
					function() {
						socket.send('file:' + $('#filename').val());
					});

			$("#filter").change(
					function() {
						socket.send('filter:' + $('#filter').val());
					});

		socket = new WebSocket("ws://localhost:8080/logservice");

socket.onopen = function() {
 $("#footer2").before("<div class='welcome' >Соединение установлено.</div>");
		socket.send('file:' + $('#filename').val());
		socket.send('filter:' + $('#filter').val());

};

socket.onclose = function(event) {
  if (event.wasClean) {
    $("#footer2").prepend('Соединение закрыто чисто');
  } else {
     $("#footer2").prepend('Обрыв соединения'); // например, "убит" процесс сервера
  }
   $("#footer2").prepend('Код: ' + event.code + ' причина: ' + event.reason);
};

socket.onmessage = function(e) {
				$("#footer2").before(e.data);
				var len = $('#main').text().length;
				if (len > 140000) {
					var temp = $('#main').html().substring(40000);
					$('#main').html(temp);
				}
				document.getElementById('footer2').scrollIntoView({
					behavior : "smooth"
				});
};

socket.onerror = function(error) {
   $("#footer2").prepend("Ошибка " + error.message);
};

		});

